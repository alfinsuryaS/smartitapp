
import * as React from 'react';
import { Provider } from 'react-redux';
import store from './src/app/index'
import Router from './src/app/routes/routes'

export default App = () => (
  <Provider store={store}>
    <Router />
  </Provider>
)