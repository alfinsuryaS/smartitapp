import * as React from 'react'
import { createStackNavigator, createBottomTabNavigator } from 'react-navigation'
import { createReduxContainer } from 'react-navigation-redux-helpers'
import { connect } from 'react-redux'
import { Media as media } from '../common/index.common'
import { Image } from 'react-native'
import { Theme } from '../common/index.common'


/* Screen */
import LoginScreen from '../components/login/index.login'
import DashboardScreen from '../components/dashboard/index.dashboard'
import JadwalScreen from '../components/jadwal/index.jadwal'
import LokasiScreen from '../components/lokasi/index.lokasi'
import PesananScreen from '../components/pesanan/index.pesanan'
import ProfileScreen from '../components/profile/index.profile'


const iconMenuActive:any = (navigation:any, focused:any) => {
    const { routeName } = navigation.state
    let imgActive;

    if (routeName === 'Dashboard') {
        imgActive = focused ? media.menu.home.active : media.menu.home.default;
    } else if (routeName === 'Jadwal') {
        imgActive = focused ? media.menu.home.active : media.menu.jadwal.default;
    }
    else if (routeName === 'Lokasi') {
        imgActive = focused ? media.menu.home.active : media.menu.lokasi.default;
    }
    else if (routeName === 'Pesanan') {
        imgActive = focused ? media.menu.home.active : media.menu.pesanan.default;
    }
    else if (routeName === 'Profile') {
        imgActive = focused ? media.menu.home.active : media.menu.profile.default;
    }

    return <Image style={{ resizeMode: 'center', width: 20, height: 30 }} source={imgActive} />
}

const TabNavigator = createBottomTabNavigator(
    {
        Dashboard: { screen: DashboardScreen },
        Jadwal: { screen: JadwalScreen },
        Lokasi: { screen: LokasiScreen },
        Pesanan: { screen: PesananScreen },
        Profile: { screen: ProfileScreen },
    },
    {
        initialRouteName: 'Dashboard',
        defaultNavigationOptions: ({ navigation }) => ({
            tabBarIcon: ({ focused }) =>
                iconMenuActive(navigation, focused)
        }),
        tabBarOptions: {
            activeTintColor: Theme.ACCENT_COLOR,
            inactiveTintColor: Theme.PRIMARY_COLOR,
            style: {
                height: 63,
                backgroundColor: '#fff',
                borderTopColor: Theme.PRIMARY_COLOR,
                paddingTop: 5,
                paddingBottom: 8

            }
        }
    }
)

const AppNavigator = createStackNavigator(
    {
        //need splash screen here
        Home: TabNavigator,
        Authorized: LoginScreen,
    },
    {
        initialRouteName: 'Authorized',
        mode: 'card',
        defaultNavigationOptions: ({ navigation }) => ({
            headerBackTitle: null,
            header: null,
            gesturesEnabled: navigation.getParam('Home') ? true : false
        }),
    }
)


const RouteContainer = createReduxContainer(AppNavigator);

const MapStateToProps = (state:any) => ({
    state: state.navigation,
})

export default connect(MapStateToProps)(RouteContainer)

