
import { createStore, applyMiddleware } from 'redux'
import { createReactNavigationReduxMiddleware } from 'react-navigation-redux-helpers'
import RootReducer from './rootReducer'


/* Nav MIiddleware */
const NavMiddleware = createReactNavigationReduxMiddleware<any>(
    state => state.navigation,
);

const store = createStore(
    RootReducer,
    applyMiddleware(NavMiddleware)
)

export default store