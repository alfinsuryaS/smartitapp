
export const LOGGED_IN = 'LOGGED_IN';

interface navProps {
    user: any
}

export const loggedInAction = (user:navProps) => {
    return {
        type: LOGGED_IN,
        payload: user
    }
}
