
/* Common style */

export default {
    DARKCOLOR: '#0f003c',
    PRIMARY_COLOR: '#b6b6b6',
    ACCENT_COLOR: '#3f3363'
}