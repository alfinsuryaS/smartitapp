
const IMG_PATH_FILE: string = '../media/image/'
const ICON_PATH_FILE: string = '../media/icon/'

const Media = {
    LoginScreen: {
        background: require(`${IMG_PATH_FILE}background-login.png`),
        headerLogo: require(`${IMG_PATH_FILE}login-screen-logo.png`),
    },

    menu: {
        home: {
            default: require(`${ICON_PATH_FILE}home-menu-icon.png`),
            active: require(`${ICON_PATH_FILE}home-menu-active-icon.png`),
        },
        jadwal: {
            default:  require(`${ICON_PATH_FILE}jadwal-menu-icon.png`),
        },
        lokasi: {
            default: require(`${ICON_PATH_FILE}lokasi-menu-icon.png`),
        },
        pesanan: {
            default: require(`${ICON_PATH_FILE}pesanan-menu-icon.png`),
            listPesanan: require(`${ICON_PATH_FILE}icon-pesanan.png`),
        },
        profile: {
            default:  require(`${ICON_PATH_FILE}profile-menu-icon.png`),
        }
    }
}

export default Media