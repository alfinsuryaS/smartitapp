import { combineReducers } from 'redux'
import { createNavigationReducer } from 'react-navigation-redux-helpers'
import AuthReducer from './reducers/navigation.reducer'
import Routes from './routes/routes'

const NavReducer:any = createNavigationReducer(Routes);

export default combineReducers({
    navigation: NavReducer,
    userAuth: AuthReducer,
})