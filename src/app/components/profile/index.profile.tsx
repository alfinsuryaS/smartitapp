
import React from 'react'
import { Text } from 'react-native'
import { FlexLayout } from '../global/index.global'

const ProfileScreen = () => (
    <FlexLayout justifyContent="center" addStyle={{ backgroundColor: '#f7f7f9' }}>
        <Text>Profile Screen</Text>
    </FlexLayout>
)

export default ProfileScreen