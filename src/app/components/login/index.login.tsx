
import React from 'react'
import { ImageBackground, View, Alert } from 'react-native'
import { NavigationScreenOptions, NavigationScreenComponent } from 'react-navigation'
import { Container, FlexLayout, Center } from '../global/index.global'
import { Media } from '../../common/index.common'
import { NavInterface, State, listInterface } from './login.interface';
import styles from './styles/index.style'
import HeaderLogin from './header.login'
import FormLogin from './form.login'
import FooterLogin from './footer.login'

const Login: NavigationScreenComponent<NavInterface> = ({ navigation }) => {
    //Hide the warning 
    console.disableYellowBox = true;

    const [state, setState]: any = React.useState<State>({
        text: '',
        errorMessage: false
    });

    //Default Values
    const listPropValue: Array<listInterface> = [
        {
            labelValue: 'Company ID',
            placeholder: 'Your company id',
            textInputType: 'none',
            secureText: false
        },
        {
            labelValue: 'Username',
            placeholder: 'Your username',
            textInputType: 'name',
            secureText: false
        },
        {
            labelValue: 'Password',
            placeholder: 'Your password',
            textInputType: 'password',
            secureText: true
        },
    ]


    const _handleSubmit = () => {
        if (state.text === '') Alert.alert("Your data is empty!")
        else navigation.navigate('Home');
    }


    return (
        <View style={styles.main}>
            <ImageBackground source={Media.LoginScreen.background} style={styles.imageBg}>
                <FlexLayout justifyContent="center">
                    <Container addStyle={{ position: 'relative' }}>
                        {/* Header */}
                        <Center>
                            <HeaderLogin />
                        </Center>

                        {/* Login Form */}
                        <FormLogin
                            lists={listPropValue}
                            displayErrorMessage={state.errorMessage}
                            onPress={_handleSubmit}
                            onChangeInput={(text: string) => setState({ text })}
                            value={state.text}
                        />
                    </Container>

                    <FooterLogin />
                </FlexLayout>
            </ImageBackground>
        </View >
    )
}

Login.navigationOptions = {
    gesturesEnabled: false,
    swipeEnabled: false,
} as NavigationScreenOptions;

export default Login
