import * as React from 'react'
import { View, Text } from 'react-native'
import styles from './styles/footer.login.style'

const FooterLogin = () => (
    <View style={styles.appVersionContainer}>
        <Text style={styles.smallText}>v.0.9.4.3</Text>
    </View>
)

export default FooterLogin