
import { NavigationScreenProp, NavigationScreenProps } from 'react-navigation'


// Navigation
export interface NavInterface extends NavigationScreenProps {
    navigation: NavigationScreenProp<any, any>
}

//For state
export interface State {
    text: string,
    errorMessage: boolean
}

//For default values
export interface listInterface {
    labelValue: string,
    placeholder: string,
    textInputType: string,
    secureText: boolean
}

//Form
export interface FormInterface {
    value: string,
    lists: any,
    onChangeInput?: any,
    displayErrorMessage?: boolean,
    onPress: () => void,
}

