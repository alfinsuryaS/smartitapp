
import { StyleSheet, Dimensions } from 'react-native'
import { Theme } from '../../../common/index.common'

export default StyleSheet.create({
    main:{
        backgroundColor: Theme.DARKCOLOR,
        position: 'relative',
    },
    imageBg: {
        width: Dimensions.get('window').width,
        height: Dimensions.get('window').height,
        resizeMode: 'stretch'
    },
})