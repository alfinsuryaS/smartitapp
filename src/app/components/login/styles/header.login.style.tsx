
import { StyleSheet, Dimensions } from 'react-native'

const DIMENSION_WIDTH =  Dimensions.get('window').width;

export default StyleSheet.create({
    headerContainer: {
        width: DIMENSION_WIDTH - 80,
        height: '15%',
        marginBottom: '18%'
    },
    brandContainer: {
        width: DIMENSION_WIDTH - 160,
    },
    img: {
        width: '100%',
        height: '100%',
        resizeMode: 'contain',
        justifyContent: 'center',
        alignItems: 'center',
    },
    text: {
        color: '#fff',
        textAlign: 'center',
        marginTop: 6,
    }
})