
import { StyleSheet, Dimensions } from 'react-native'


export default StyleSheet.create({
    main: {
        justifyContent: 'center',
        marginLeft: 10,
        marginRight: 10
    },
    submitContainer: {
        display: 'flex',
        alignItems: 'center',
        marginTop: 20
    },
    button: {
        width: '50%',
        justifyContent: 'center',
        borderRadius: 12
    },
    buttonText: {
        color: '#fff',
    },
    formGroup: {
        width: Dimensions.get('screen').width - 70,
        marginBottom: 20,
    },
    inputStyle: {
        height: 40,
        borderColor: '#fff',
        borderBottomWidth: 1,
        color: '#fff',
    },
    labelColor: {
        color: "#fff"
    }
})