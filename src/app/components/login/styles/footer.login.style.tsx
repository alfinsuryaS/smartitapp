
import { StyleSheet, Platform } from 'react-native'

export default StyleSheet.create({
    appVersionContainer: {
        position: 'absolute',
        bottom: Platform.OS === 'ios' ? 40 : 60,
        left: 0,
        right: 0,
        justifyContent: 'center',
        alignItems: 'center',
    },
    smallText: {
        fontSize: 11,
        color: "rgba(255, 255, 255, .4)",
        justifyContent: 'center',
        textAlign: 'center',
    }
})