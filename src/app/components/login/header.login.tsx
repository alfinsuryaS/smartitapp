
import * as React from 'react'
import { View, Image, Text } from 'react-native'
import { Media } from '../../common/index.common'
import { Center } from '../global/index.global'
import styles from './styles/header.login.style'

const HeaderLogin = () => {
    return (
        <View style={styles.headerContainer}>
            <Center>
                <View style={styles.brandContainer}>
                    <Image style={styles.img} source={Media.LoginScreen.headerLogo} />
                </View>
            </Center>
            <Text style={styles.text}>Bersama kita bisa melangkah lebih jauh.</Text>
        </View>
    )
}


export default HeaderLogin