
import * as React from 'react'
import { View } from 'react-native'
import { Button, Text, Item, Icon, Input } from 'native-base'
import styles from './styles/form.login.style'
import { FormInterface } from './login.interface'


const FormLogin:React.FC<FormInterface> = ({ 
    onChangeInput, 
    onPress,
    displayErrorMessage, 
    lists,
    value,
 }
) => {

    return (
        <View style={styles.main}>
            {
                lists.map((item:any, index:any) => (
                    <View style={styles.formGroup} key={index}>
                        <Text style={styles.labelColor}>{item.labelValue}</Text>
                        <Item error={false}>
                            <Input
                                textContentType={item.textInputType}
                                placeholder={item.placeholder}
                                style={styles.inputStyle}
                                placeholderTextColor='rgba(255, 255, 255, .6)'
                                secureTextEntry={item.secureText}
                                onChangeText={onChangeInput}
                                value={value}
                            />
                            {
                                displayErrorMessage ? (
                                    <Icon name="close-circle" />
                                ) : null
                            }
                        </Item>
                    </View>
                ))
            }

            <View style={styles.submitContainer}>
                <Button style={styles.button} bordered light onPress={onPress}>
                    <Text style={styles.buttonText}>Masuk</Text>
                </Button>
            </View>
        </View>
    )
}

export default FormLogin