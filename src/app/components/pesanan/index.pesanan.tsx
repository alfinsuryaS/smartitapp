
import React from 'react'
import { ScrollView } from 'react-native'
import { Container, FlexLayout } from '../global/index.global'
import HeaderPesanan from './header.pesanan'
import ListPesanan from './list.pesanan'
import { ContextInterface, Generic } from './pesanan.interface'


const PesananScreen = () => {
    const Context = React.createContext<Array<ContextInterface>>([
        {
            timeOrder: 'Bulan Ini',
            namePeopleOrder: 'Heru Setiawan Office',
            itemCount: '5 item',
            date: '2019-09-20',
            time: '16:26:34'
        },
        {
            timeOrder: 'Bulan Ini',
            namePeopleOrder: 'Heru Setiawan Office',
            itemCount: '5 item',
            orderKey: 'eqwieqw0e',
            date: '2019-09-20',
            time: '16:26:34'
        }
    ]);

    //Consume this context
    const ContextConsumer = React.useContext(Context);

    //Generic Component
    const GenericListPesanan: React.FC<Generic> = (
        { items, itemDisplay }) => items.map(itemDisplay)

    //Default values
    let timeOrderLists:string[] = ['Bulan Ini', 'Agustus 2019', 'September 2018'];

    return (
        <FlexLayout addStyle={{ flex: 1, backgroundColor: '#f7f7f9' }} justifyContent="flex-start">
            <Container addStyle={{ marginTop: 40 }}>
                <HeaderPesanan />
                <ScrollView style={{ marginTop: 30 }}>
                    <GenericListPesanan
                        items={timeOrderLists}
                        itemDisplay={(item, key) =>
                            <ListPesanan 
                                items={ContextConsumer} 
                                timeOrder={item} key={key}
                            />
                        }
                    />
                </ScrollView>
            </Container>
        </FlexLayout>
    )
}

export default PesananScreen
