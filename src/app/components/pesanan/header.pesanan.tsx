import * as React from 'react';
import { Text, Alert } from 'react-native'
import { FlexLayout } from '../global/index.global'
import style from './styles/header.pesanan.style'
import { Icon, Input, Item, Button } from 'native-base'

const HeaderPesanan: React.FC = () => {
    return (
        <React.Fragment>
            {/* <!--  Content --> */}
            <FlexLayout flexDirection="row" justifyContent="space-between" addStyle={{ marginBottom: 2, }}>
                <Text style={style.headerTitle}>Pesanan</Text>

                <Button activeOpacity={1} style={style.selectButton} onPress={() => Alert.alert("Hello")}>
                    <Text style={style.dropdownText}>Semua</Text>
                    <Icon type="Ionicons" style={style.icon} name="md-arrow-dropdown" />
                </Button>
            </FlexLayout>

            {/* <!--  Input Search --> */}
            <Item rounded style={style.formItem}>
                <Input placeholder="Cari Tempat..." style={style.input} placeholderTextColor="#484848" />
                <Icon active name="search" style={style.iconSearch} />
            </Item>
        </React.Fragment >
    )
}

export default HeaderPesanan