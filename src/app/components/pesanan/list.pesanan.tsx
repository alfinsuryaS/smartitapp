
import * as React from 'react';
import { Text, View, Image, Alert } from 'react-native'
import { FlexLayout } from '../global/index.global'
import { Media as media } from '../../common/index.common'
import style from './styles/list.pesanan.style'
import { listProps } from './pesanan.interface';

const ListPesanan: React.FC<listProps> = ({ items, timeOrder, key }) => (
    <View style={style.container} key={key}>
        <Text style={style.label}>{timeOrder}</Text>
        {
            items.map((item: any, key: number) =>
                <FlexLayout flexDirection="row" key={key} justifyContent="space-between" addStyle={style.list} onPress={() => Alert.alert("Hello")}>
                    <Image source={media.menu.pesanan.listPesanan} style={style.img} />
                    <View style={{ flexGrow: 2, marginLeft: 10 }}>
                        <Text style={style.title}>{item.namePeopleOrder}</Text>
                        <Text style={style.textItemCount}>
                            {item.itemCount} {item.orderKey ? (<Text>• {item.orderKey}</Text>) : null}
                        </Text>
                    </View>
                    <View>
                        <Text style={style.textTime}>{item.date}</Text>
                        <Text style={style.textTime}>{item.time}</Text>
                    </View>
                </FlexLayout>
            )
        }
    </View>
)

export default ListPesanan