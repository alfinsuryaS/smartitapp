
import { StyleSheet } from 'react-native'
import { Theme } from '../../../common/index.common'

export default StyleSheet.create({
    headerTitle: {
        fontSize: 30,
        fontWeight: "700",
        marginBottom: 5,
        color: Theme.DARKCOLOR,
        letterSpacing: -.4
    },
    dropdownText: {
        fontSize: 14,
        color: Theme.DARKCOLOR,
    },
    selectButton: {
        backgroundColor: 'transparent',
        width: 70,
    },
    icon: {
        marginTop: 2,
        fontSize: 25,
        marginLeft: 5,
        color: Theme.DARKCOLOR
    },
    formItem: {
        borderColor: '#e0e0e0',
        borderWidth: 3,
        borderRadius: 10,
        fontSize: 5,
    },
    input: {
        fontSize: 14,
        marginHorizontal: 5,
        marginBottom: 3,
        height: 31,
    },
    iconSearch: {
        fontSize: 18,
        marginRight: 5,
    }
})