
import { StyleSheet } from 'react-native'
import { Theme } from '../../../common/index.common'

export default StyleSheet.create({
    container:{
        marginBottom: 25,
    },
    list: {
        padding: 10,
        backgroundColor: '#fff',
        borderRadius: 10,
        marginTop: 10,
        marginBottom: 3,
        minHeight: 50,
    },
    label: {
        color: Theme.DARKCOLOR,
        fontWeight: '700',
        fontSize: 12,
    },
    img: {
        resizeMode: 'center',
        width: 50,
        height: 50,
    },
    title: {
        fontSize: 15,
        lineHeight: 23,
        color: Theme.DARKCOLOR,
    },
    textItemCount: {
        fontSize: 10,
        color: Theme.DARKCOLOR,
    },
    textTime: {
        fontSize: 8,
        textAlign: 'right',
        color: Theme.DARKCOLOR,
        lineHeight: 12,
    }
})