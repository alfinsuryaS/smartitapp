


export interface ContextInterface {
    timeOrder: string,
    namePeopleOrder: string,
    itemCount: string,
    date: string,
    time: string,
    orderKey?: string,
}

export interface listProps {
    items: any,
    timeOrder: string,
    key: number,
}

export interface Generic {
    items: any, 
    itemDisplay: (item: any, key:number) => JSX.Element | null
}