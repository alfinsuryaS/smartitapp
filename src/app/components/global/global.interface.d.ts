

//Global Comp Interface



type JustifyContent =
    | "center"
    | "flex-start"
    | "space-between";

type FlexDirection =
    | "row"
    | "column";

export interface FlexLayoutInterface {
    children: React.ReactNode,
    justifyContent?: JustifyContent,
    flexDirection?: FlexDirection,
    addStyle?: object,
    onPress?: any,
}

export interface contentContentInterface {
    children: React.ReactNode,
}
export interface ContainerInterface {
    children: React.ReactNode,
    addStyle?: object
}


