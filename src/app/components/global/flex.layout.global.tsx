
import * as React from 'react'
import { View, StyleSheet, } from 'react-native'
import { FlexLayoutInterface } from './global.interface'

const FlexStyleContainer: React.FC<FlexLayoutInterface> = (
    { children, addStyle, justifyContent, flexDirection, onPress }) => {
    const styles = StyleSheet.create({
        container: {
            justifyContent: justifyContent,
            alignItems: 'center',
            flexDirection: flexDirection,
        }
    })

    return (
        <View style={[styles.container, addStyle]} onTouchStart={onPress}>
            {children}
        </View>
    )
}

export default FlexStyleContainer
