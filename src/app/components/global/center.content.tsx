
import * as React from 'react'
import { View } from 'react-native'
import { contentContentInterface } from './global.interface'
import styles from './styles/center.content.style'

const CenterContainer: React.FC<contentContentInterface> = ({ children }) => (
    <View style={styles.center}>
        {children}
    </View>
)

export default CenterContainer