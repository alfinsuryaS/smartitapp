
import React from 'react'
import { TouchableNativeFeedback, Text, View } from 'react-native'

const Button = () => {
    return (
        <TouchableNativeFeedback background={TouchableNativeFeedback.SelectableBackground()}>
            <View style={{ width: 150, height: 100, backgroundColor: 'red' }}>
                <Text style={{ margin: 30 }}>Button</Text>
            </View>
        </TouchableNativeFeedback>
    )
}


export default Button