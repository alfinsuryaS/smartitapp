

import { StyleSheet, Platform } from 'react-native'

export default StyleSheet.create({
    container: {
        marginTop: Platform.OS === 'ios' ? 20 : 0,
        paddingHorizontal: 20,
        width: '100%'
    }
})

