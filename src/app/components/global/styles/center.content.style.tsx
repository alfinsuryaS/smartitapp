

import { StyleSheet } from 'react-native'


export default StyleSheet.create({
    center: {
        display: 'flex',
        alignItems: 'center'
    }
})