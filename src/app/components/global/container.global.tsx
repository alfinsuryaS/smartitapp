
import React from 'react'
import { View } from 'react-native'
import { ContainerInterface } from './global.interface'
import styles from './styles/container.style'

const Container:React.FC<ContainerInterface> = ({ children, addStyle }) => {
    return (
        <View style={[styles.container, addStyle]}>
            {children}
        </View>
    )
}


export default Container