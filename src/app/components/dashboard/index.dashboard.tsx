
import React from 'react'
import { Text } from 'react-native'
import { FlexLayout } from '../global/index.global'

const DashboardScreen = () => (
    <FlexLayout justifyContent="center" addStyle={{ backgroundColor: '#f7f7f9' }}>
        <Text>Dashboard Screen</Text>
    </FlexLayout>
)

DashboardScreen.navigationOptions = () => ({
    gesturesEnabled: false,
    swipeEnabled: false
});


export default DashboardScreen