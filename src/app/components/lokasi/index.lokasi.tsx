
import React from 'react'
import { Text } from 'react-native'
import { FlexLayout } from '../global/index.global'

const LokasiScreen = () => (
    <FlexLayout justifyContent="center" addStyle={{ backgroundColor: '#f7f7f9' }}>
        <Text>Lokasi Screen</Text>
    </FlexLayout>
)

export default LokasiScreen