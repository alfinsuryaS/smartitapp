

const INIITIAL_STATE = {
    user: ''
}

const AuthReducer = (state = INIITIAL_STATE, action:any) => {
    switch (action.type) {
        case 'USER_LOGGED_IN':
            return {
                ...state,
                user: action.payload,
            }
        default:
            return state
    }
}

export default AuthReducer

